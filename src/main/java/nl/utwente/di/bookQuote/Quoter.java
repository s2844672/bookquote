package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private final HashMap<String, Double> bookMap = new HashMap<>();

    public Quoter(){
        bookMap.put("1", 10.0);
        bookMap.put("2", 45.0);
        bookMap.put("3", 20.0);
        bookMap.put("4", 35.0);
        bookMap.put("5", 50.0);

    }


    double getBookPrice(String isbn) {
        if (bookMap.containsKey(isbn)) {
            return bookMap.get(isbn);
        }
        return 0.0;
    }
}
